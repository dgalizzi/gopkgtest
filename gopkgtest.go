package gopkgtest

// Greet says hello to name
func Greet(name string) string {
	return "Hello " + name
}
